// C imports
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

// SDL2 imports
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_ttf.h>

#include "snakebite.h"
#include "functions.h"

// Cleanup routine

void cleanupApplication() {
    printf("\nShutting down SDL2...\n");
    SDL_VideoQuit();
    SDL_Quit();

    printf("Shutting down SDL2 TTF...\n");
    TTF_Quit();

    printf("\nApplication cleanup succesfull!\nHave a good day.\n\n");

    return;
}

// Create a texture on a renderer given a file name and a renderer instance

SDL_Texture * createTexture(const char* filename, SDL_Renderer * renderInstance) {
    // Load the bitmap grahpic into a surface
    SDL_Surface* surface = SDL_LoadBMP(filename);

    if (surface == NULL) {
        printf("Could not create texture surface: %s\n", SDL_GetError());
        exit(1);
    } else {
        // Create the texture from the surface
        SDL_Texture* texture = SDL_CreateTextureFromSurface(renderInstance, surface);
        if (texture == NULL) {
            printf("Error creating texture: %s\n", SDL_GetError());
            exit(1);
        }

        // Free obsolete surface and return the texture
        SDL_FreeSurface(surface);
        return texture;
    }

    return NULL;
}

// Calculate a field index to pixel by either height or width

int calcPointToPixel(const int point, const bool isHeight) {
    if (isHeight) {
        return (point * FIELD_HEIGHT) + (FIELD_OFFSETY + UI_OFFSET_Y);
    } else {

        return (point * FIELD_WIDTH) +FIELD_OFFSETX;
    }
}

// Return a field index based on a fields pX and pY coordinates

int getFieldIndex(const int pX, const int pY, const gameLogic * logic) {

    return pX + (logic->fieldsX * pY);
}

// Get a random unused field index or return -1 if none is found

int getAvailableRandomFieldIndex(const gameLogic* logic, const snakeDef * snakes) {
    int length = logic->fieldCount - logic->itemCount;

    for (int i = 0; i < logic->snakes; i++) {
        if (snakes[i].isInGame) {
            length -= snakes[i].length;
        }
    }

    if (length <= 0) {
        return -1;
    }

    int randomFieldArray[length + 1];
    int randomLength = 0;

    int j = 0, k = 0;
    bool isAvailable = false;

    for (int i = 0; i < logic->fieldCount; i++) {

        // If its a wall tile, dont add it to the available list
        if (logic->fields[i].isWall) {
            continue;
        }

        // Set the tile initialle to be available
        isAvailable = true;

        // Check if any item is at this location
        for (j = 0; j < logic->itemCount; j++) {
            if (i == logic->itemFields[j]) {
                continue;
                break;
            }
        }

        // Check if any snake is at this tile
        for (k = 0; k < logic->snakes; k++) {
            if (!snakes[k].isInGame) {
                continue;
            }

            for (j = 0; j < snakes[k].length; j++) {
                if (i == snakes[k].snakeFields[j]) {
                    isAvailable = false;
                    break;
                }
            }

            if (!isAvailable) {
                break;
            }
        }

        // If its available, at it to the random field array list
        if (isAvailable) {
            randomFieldArray[randomLength] = i;
            ++randomLength;
        }
    }

    return randomFieldArray[ rand() % (randomLength - 1) ];
}

// Place a snake on the game field

void placeSnake(const gameLogic* logic, snakeDef* snakes, const int snakeIndex, const int fieldX, const int fieldY) {
    // Snake placement
    snakeDef* snake = &snakes[snakeIndex];
    if (fieldX == -1 && fieldY == -1) {
        if (snakeIndex == 0) {
            snake->pX = (rand() % (logic->fieldsX - (12 + snake->length))) + (snake->length + 8);
            snake->pY = (rand() % (logic->fieldsY - 6)) + 4;
        } else {
            snake->pX = (rand() % (logic->fieldsX - (12 + snake->length))) + (snake->length + 8);
            snake->pY = (snakes[0].pY + 2) <= (logic->fieldsY - 2) ? snakes[0].pY + 2 : snakes[0].pY + 1;
        }
    } else {
        snake->pX = fieldX;
        snake->pY = fieldY;
    }

    // Inititalze snake
    for (int i = 0; i < snake->length; i++) {
        snakePart* part = &snake->tail[i];
        part->orientation = LEFT;
        part->pX = snake->pX + i;
        part->pY = snake->pY;
        part->corner = 0;
        part->location = (SDL_Rect){calcPointToPixel(part->pX, false), calcPointToPixel(part->pY, true), FIELD_WIDTH, FIELD_HEIGHT};

        // Setup logic snakeFields with field index
        snake->snakeFields[i] = getFieldIndex(part->pX, part->pY, logic);

        if (i == 0) {
            part->previous = NULL;
        } else {
            part->previous = &snake->tail[i - 1];
        }

        if ((i + 1) < snake->length) {
            part->next = &snake->tail[i + 1];
        } else {

            part->next = NULL;
        }
    }
}

// Extend the snake by one tail part when it was eating

void extendSnake(const gameLogic* logic, snakeDef* snake, const int headPrevPX, const int headPrevPY) {
    // Allocated memory and move existing parts in memory one after
    snake->tail = (snakePart*) realloc(snake->tail, sizeof (snakePart) * (snake->length + 1));
    if (snake->tail == NULL) {
        printf("ERROR: Could not allocated memory for snake part.\n");
        exit(1);
    }

    // Move existing snaike tail parts one ahead
    memmove(&snake->tail[2], &snake->tail[1], sizeof (snakePart) * (snake->length - 1));

    // New snake part definition
    snakePart* newPart = &snake->tail[1];
    newPart->pX = headPrevPX;
    newPart->pY = headPrevPY;
    newPart->orientation = snake->tail[0].orientation;
    newPart->location = (SDL_Rect){calcPointToPixel(newPart->pX, false), calcPointToPixel(newPart->pY, true), FIELD_WIDTH, FIELD_HEIGHT};
    newPart->corner = 0;

    // Update existing snake parts with new pointers in memory
    for (int i = 1; i <= snake->length; i++) {
        snake->tail[i].next = &snake->tail[i + 1];
        snake->tail[i].previous = &snake->tail[i - 1];
    }

    // Update pointers of snake tail tail
    snake->tail[snake->length].previous = &snake->tail[snake->length - 1];
    snake->tail[snake->length].next = NULL;

    // Update pointer of snake head next element
    snake->tail[0].next = &snake->tail[1];

    // Update logic of used snake fields
    snake->snakeFields = (int*) realloc(snake->snakeFields, sizeof (int) * (snake->length + 1));

    if (snake->snakeFields == NULL) {
        printf("ERROR: Could not allocated memory for logic snakefields.\n");
        exit(1);
    }

    // Update snakefields with new field index
    snake->snakeFields[snake->length] = getFieldIndex(newPart->pX, newPart->pY, logic);

    // Increase snake length
    ++snake->length;
}

// Reset the game on reset

void resetGame(gameLogic* logic, snakeDef * snakes) {
    int i = 0;

    // Reset game fields
    for (i = 0; i < logic->fieldCount; i++) {
        logic->fields[i].containsItem = false;
        logic->fields[i].isWall = false;
        logic->fields[i].itemIndex = 0;

        if (logic->fields[i].pY == 0 || logic->fields[i].pY == logic->fieldsY - 1 || logic->fields[i].pX == 0 || logic->fields[i].pX == logic->fieldsX - 1) {
            logic->fields[i].isWall = true;
        } else {
            logic->fields[i].isWall = false;
        }
    }

    // Reset item holders
    for (i = 0; i < (MAX_ITEMS + 1); i++) {
        logic->itemFields[i] = 0;
        logic->itemAge[i] = 0;
        logic->items[i] = 0;
    }

    // Reset logic
    logic->itemCount = 0;
    logic->lastFood = FOOD_DELAY;
    logic->lastPowerUp = POWERUP_DELAY;
    logic->gameSpeed = INITIAL_GAME_SPEED;
    logic->playTime = 0;
    logic->score = 0;
    logic->itemAdditions = 0;
    logic->level = 0;
    logic->gameSpeedRamp = 1.0;
    logic->startTime = time(NULL);

    // Reset snakes
    for (int i = 0; i < 2; i++) {
        snakes[i].ateLength = 0;
        snakes[i].hasNewDirection = false;
        snakes[i].hasPowerUp = false;
        snakes[i].isInGame = false;
        snakes[i].length = INITIAL_SNAKE_LENGTH;
        snakes[i].powerUp = 0;
        snakes[i].powerUpTime = 0;
    }

    if (logic->gameMode == SINGLE_PLAYER) {
        // Place snake one into a new postion and in game
        placeSnake(logic, snakes, 0, -1, -1);
        snakes[0].isInGame = true;
    } else {
        placeSnake(logic, snakes, 0, -1, -1);
    }
}

/*
 * References:
 * http://stackoverflow.com/questions/14279242/read-bitmap-file-into-structure
 * https://de.wikipedia.org/wiki/Windows_Bitmap
 * */

/*
typedef struct tagBITMAPFILEHEADER {
    Uint16 bfType; //specifies the file type
    Uint32 bfSize; //specifies the size in bytes of the bitmap file
    Uint32 bfReserved1; //reserved; must be 0
    Uint32 bOffBits; //species the offset in bytes from the bitmapfileheader to the bitmap bits
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER {
    DWORD biSize; //specifies the number of bytes required by the struct
    LONG biWidth; //specifies width in pixels
    LONG biHeight; //species height in pixels
    WORD biPlanes; //specifies the number of color planes, must be 1
    WORD biBitCount; //specifies the number of bit per pixel
    DWORD biCompression; //spcifies the type of compression
    DWORD biSizeImage; //size of image in bytes
    LONG biXPelsPerMeter; //number of pixels per meter in x axis
    LONG biYPelsPerMeter; //number of pixels per meter in y axis
    DWORD biClrUsed; //number of colors used by th ebitmap
    DWORD biClrImportant; //number of colors that are important
} BITMAPINFOHEADER;
 */

// Old bmp loading attempt

/*
bool loadLevel(const int level, gameLogic* logic, snakeDef *snakes) {
    char filename[128];
    sprintf(filename, "lvls/level_%d.bmp", level);

    unsigned char dataBuffer[32] = "\0";
    unsigned char* ptrC = (unsigned char*) &dataBuffer;

    int width = 0, height = 0, offset = 0, bitCount = 0, colorTable = 0, colorCount = 0, bitCompression = 0;
    FILE* levelFile = fopen(filename, "rb");
    if (levelFile != NULL) {

        fseek(levelFile, 10, SEEK_SET);
        fread(ptrC, sizeof (char), 4, levelFile);
        offset = (int) *ptrC;

        fseek(levelFile, 18, SEEK_SET);
        fread(ptrC, sizeof (char), 4, levelFile);
        width = (int) *ptrC;

        fseek(levelFile, 22, SEEK_SET);
        fread(ptrC, sizeof (char), 4, levelFile);
        height = (int) *ptrC;

        fseek(levelFile, 28, SEEK_SET);
        fread(ptrC, sizeof (char), 2, levelFile);
        bitCount = (int) *ptrC;

        fseek(levelFile, 30, SEEK_SET);
        fread(ptrC, sizeof (char), 4, levelFile);
        bitCompression = (int) *ptrC;

        fseek(levelFile, 46, SEEK_SET);
        fread(ptrC, sizeof (char), 4, levelFile);
        colorTable = (int) *ptrC;

        fseek(levelFile, 50, SEEK_SET);
        fread(ptrC, sizeof (char), 4, levelFile);
        colorCount = (int) *ptrC;

        printf("Level information:\nWidth: %i\nHeight: %i\nBPP: %i\nData Offset: %i\nColor Table used: %i\nColors in table: %i\nCompression: %i\n\n",
                width, height, bitCount, offset, colorTable, colorCount, bitCompression);
        //fseek(levelFile, 0, SEEK_END);
        //int levelFileEnd = ftell(levelFile);
        //rewind(levelFile);

        fseek(levelFile, offset, SEEK_SET);

        int junkBytes = 4 - (width % 4);
        printf("Junk bytes: %i\n", junkBytes);

        unsigned int r = 0, g = 0, b = 0, a = 0;

        for (int y = 1; y <= height; y++) {
            for (int x = 1; x <= width; x++) {
                //b = fgetc(levelFile);

                fread(ptrC, sizeof (Uint8), 1, levelFile);
                a = (int) *ptrC;

                fread(ptrC, sizeof (Uint8), 1, levelFile);
                b = (int) *ptrC;
                //g = fgetc(levelFile);

                fread(ptrC, sizeof (Uint8), 1, levelFile);
                g = (int) *ptrC;
                //r = fgetc(levelFile);

                fread(ptrC, sizeof (Uint8), 1, levelFile);
                r = (int) *ptrC;

                printf("R%i G%i B%i A%i\n", r, g, b, a);
            }

            if (junkBytes != 0) {
                fseek(levelFile, junkBytes * 4, SEEK_CUR);
            }
        }

        fclose(levelFile);
        return true;
    }

    return false; // false
} */

bool loadLevel(const int level, gameLogic* logic, snakeDef *snakes) {
    char filename[128];
    sprintf(filename, "lvls/level_%d.data", level);
    printf("current level file to load: %s\n", filename);

    Uint8* ptrC = (Uint8*) malloc(sizeof (Uint8));
    Uint8 r = 0, g = 0, b = 0, a = 0;

    FILE* levelFile = fopen(filename, "r");

    bool foundSnakeLocation = false;
    int index = 0;

    if (levelFile != NULL) {

        for (int y = 0; y <= 26; y++) {
            for (int x = 0; x <= 38; x++) {
                fread(ptrC, sizeof (Uint8), 1, levelFile);
                r = (Uint8) * ptrC;

                fread(ptrC, sizeof (Uint8), 1, levelFile);
                g = (Uint8) * ptrC;

                fread(ptrC, sizeof (Uint8), 1, levelFile);
                b = (Uint8) * ptrC;

                fread(ptrC, sizeof (Uint8), 1, levelFile);
                a = (Uint8) * ptrC;

                //printf("R%i G%i B%i A%i\n", r, g, b, a);

                if (r <= 10 && g <= 10 && b <= 10) {
                    index = x + (logic->fieldsX * y);
                    logic->fields[index].isWall = true;
                } else if (r > 250 && g < 10 && b < 10) {
                    foundSnakeLocation = true;
                    placeSnake(logic, snakes, 0, x, y);
                }
            }
        }

        fclose(levelFile);
    } else {
        return false;
    }

    free(ptrC);

    if (!foundSnakeLocation) {
        printf("Snake location missing for level file \"%s\"", filename);
        return false;
    }

    return true;
}
