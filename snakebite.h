#ifndef SNAKEBITE_H
#define SNAKEBITE_H

// Version definitions
#define VERSIONNUMBER "v0.94"
#define VERSIONDATE " | 22.11.2015"

// Enums

enum fieldData {
    FIELD_WIDTH = 16,
    FIELD_HEIGHT = 16,
    FIELD_OFFSETX = 8,
    FIELD_OFFSETY = 6,
    UI_OFFSET_Y = 32
};

enum messageData {
    MSG_WALL = 0,
    MSG_BITE = 1
};

enum directionData {
    LEFT = 0,
    RIGHT = 1,
    TOP = 2,
    BOTTOM = 3,
    CLB = 1,
    CLT = 2,
    CRB = 3,
    CRT = 4
};

enum logicData {
    FOOD_DELAY = 10, //30
    FOOD_DECAY = -1,
    POWERUP_DELAY = 80,
    POWERUP_DECAY = 60,
    MAX_ITEMS = 10,
    INITIAL_SNAKE_LENGTH = 3,
    INITIAL_GAME_SPEED = 220,
    MINIMUM_GAME_SPEED = 140,
    GAME_SPEED_DECREASE = 2,
    SINGLE_PLAYER = 1,
    MULTI_PLAYER = 2
};

enum itemData {
    APPLE = 1,
    CHILLI = 2,
    STRAWBERRY = 3,
    BANANA = 4
};

// Struct definitions

typedef struct gameLogic {
    int lastFood;
    int lastPowerUp;
    int itemCount;
    int gameSpeed;
    time_t startTime;
    unsigned int playTime;
    unsigned int score;
    int itemAdditions;
    int itemFields[MAX_ITEMS + 1];
    int items[MAX_ITEMS + 1];
    int itemAge[MAX_ITEMS + 1];
    int snakes;
    int fieldsX;
    int fieldsY;
    int fieldCount;

    int level;
    int gameMode;
    float gameSpeedRamp;

    struct field* fields;
} gameLogic;

typedef struct field {
    bool containsItem;
    bool isWall;
    int itemIndex;
    int pX;
    int pY;
    SDL_Rect location;
} field;

typedef struct snakePart {
    bool isCorner;
    int corner;
    int orientation;
    int pX;
    int pY;
    SDL_Rect location;
    struct snakePart* previous;
    struct snakePart* next;
} snakePart;

typedef struct snakeDef {
    bool isInGame;
    bool hasPowerUp;
    bool hasNewDirection;
    int powerUp;
    float powerUpTime;
    int ateLength;
    int pX;
    int pY;
    int length;
    int* snakeFields;
    struct snakePart* tail;
} snakeDef;
#endif
