#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include "snakebite.h"

// Function definitions / Funktionsdefininitionen
void cleanupApplication();

SDL_Texture* createTexture(const char* filename, SDL_Renderer* renderInstance);
int calcPointToPixel(const int point, const bool isHeight);
int getFieldIndex(const int pX, const int pY, const gameLogic* logic);
int getAvailableRandomFieldIndex(const gameLogic* logic, const snakeDef* snakes);
void placeSnake(const gameLogic* logic, snakeDef* snakes, const int snakeIndex, const int fieldX, const int fieldY);
void extendSnake(const gameLogic* logic, snakeDef* snake, const int headPrevPX, const int headPrevPY);
void resetGame(gameLogic* logic, snakeDef* snakes);
bool loadLevel(const int level, gameLogic* logic, snakeDef *snakes);
#endif
