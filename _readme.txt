Dies ist ein Snake Clon von theSplit f�r die NGB Coding Challenge #2 "Aaaaahhhh Snake".

This is a snake clone written by theSplit for the NGB coding challenge #2 with the title "Aaaaahhhh Snake".

The controls are:
WASD or Arrow keys to move the snake top, left, bottom, right
R - Resets the game
P - Pause the game

NGB: http://ngb.to
NGB Gitlab: http://gitlab.com/u/ngb/
The particular snake project repository: https://gitlab.com/ngb/NGB-Snake_theSplit
