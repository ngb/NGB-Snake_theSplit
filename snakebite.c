// C imports
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

// SDL2 imports
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_ttf.h>

// Own header file includes
// Eigene Header Dateien einbinden
#include "snakebite.h"
#include "functions.h"



// Main routine
// Haupt Routine

int main(int argc, char* argv[]) {
    atexit(cleanupApplication);

    // Legal stuff (Console output)
    // Rechtliche Hinweise (Konsoleoutput)
    printf("\nWelcome to Snake(bite) - a snake clone for the ngb coding challenge #2, November 2015\n");
    printf("\nDISCLAIMER:\nThis software comes as is and there is no warranty that\nevery piece of code provided is fit for an particular purpose.\n\nAlso I cannot be held reliable for any damages occuring using this code.\n");

    // Set the application title based in conjunction with version number
    // and version date
    // Setzen des Fenstertitels basiered auf String mit angefuegter
    // Versionsnummer und Versionsdatum
    char versionTitle[64] = "Snakebite snakepit | ";
    strcat(versionTitle, VERSIONNUMBER);
    strcat(versionTitle, VERSIONDATE);

    // Start up
    // Programmstart

    // Set window width and window height
    // Setzen der Fensterbreite und Hoehe
    int windowWidth = 640;
    int windowHeight = 480;

    printf("\n\nStarting up...\n");

    // SDL2 Video Init
    // SDL2 Video Initialisierung
    printf("Initializing SDL2 video...");
    if (SDL_VideoInit(NULL) != 0) {
        printf("\nERROR: Could not initialize SDL2 video: %s\n", SDL_GetError());
        exit(1);
    } else {
        printf(" success!\n");
    }

    // SDL2_ttf Init
    // SDL2_ttf Initialisierung
    printf("Initializing TTF font library...");
    TTF_Font* rw14 = NULL;
    TTF_Font* rw58 = NULL;

    if (TTF_Init() != 0) {
        printf("\nERROR: Could not initialize TTF font library : %s\n", TTF_GetError());
        exit(1);
    } else {
        printf(" success!\n");

        // Loading font with different sizes
        // Laden der Schriftarten in verschiedenen Groessen
        rw14 = TTF_OpenFont("fonts/Raleway-Light.ttf", 14);
        if (rw14 == NULL) {
            printf("ERROR: Could not load font, disabling UI rendering...\nTTF Error:%s\n", TTF_GetError());
            exit(1);
        }

        rw58 = TTF_OpenFont("fonts/Raleway-Light.ttf", 58);

        if (rw58 == NULL) {
            printf("ERROR: Could not load font, disabling UI rendering...\nTTF Error:%s\n", TTF_GetError());
            exit(1);
        }
    }

    // Main window and mainrenderer definition
    // Hauptfenster definition und SDL Hauptrenderer Definition
    SDL_Window *snakeWindow = SDL_CreateWindow(versionTitle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight, 0);
    if (snakeWindow == NULL) {
        printf("ERROR: Could not create main application window.\n");
        exit(1);
    }

    SDL_Renderer* gfxRender = SDL_CreateRenderer(snakeWindow, -1, SDL_RENDERER_ACCELERATED || SDL_RENDERER_PRESENTVSYNC);

    // UI variables
    // Benutzerinterfacce Definitionen
    SDL_Color uiFontColor = (SDL_Color){255, 255, 255};
    char displayText[512];

    SDL_Surface* scoreboard = NULL;
    SDL_Rect scoreRect = {0, 0, 0, 0};

    SDL_Surface* timeboard = NULL;
    SDL_Rect timeRect = {0, 0, 0, 0};

    SDL_Surface* lengthboard = NULL;
    SDL_Rect lengthRect = {0, 0, 0, 0};

    SDL_Surface* deathboard = NULL;
    SDL_Rect deathRect = {0, 0, 0, 0};

    SDL_Surface* deathReasonboard = NULL;
    SDL_Rect reasonRect = {0, 0, 0, 0};

    // Snake head textures
    // Schlangenkopf Texturen
    SDL_Texture* head_l = createTexture("gfx/head_left.bmp", gfxRender);
    SDL_Texture* head_t = createTexture("gfx/head_top.bmp", gfxRender);
    SDL_Texture* head_r = createTexture("gfx/head_right.bmp", gfxRender);
    SDL_Texture* head_b = createTexture("gfx/head_bottom.bmp", gfxRender);

    // Tail textures
    // Schlangenkoerper
    SDL_Texture* tail_l = createTexture("gfx/tail_left.bmp", gfxRender);
    SDL_Texture* tail_t = createTexture("gfx/tail_top.bmp", gfxRender);
    SDL_Texture* tail_r = createTexture("gfx/tail_right.bmp", gfxRender);
    SDL_Texture* tail_b = createTexture("gfx/tail_bottom.bmp", gfxRender);
    SDL_Texture* sp_hor = createTexture("gfx/tail_horizontal.bmp", gfxRender);
    SDL_Texture* sp_ver = createTexture("gfx/tail_vertical.bmp", gfxRender);

    // Tail corners
    // Koerper Ecken
    SDL_Texture* sp_clb = createTexture("gfx/corner_lb.bmp", gfxRender);
    SDL_Texture* sp_clt = createTexture("gfx/corner_lt.bmp", gfxRender);
    SDL_Texture* sp_crb = createTexture("gfx/corner_rb.bmp", gfxRender);
    SDL_Texture* sp_crt = createTexture("gfx/corner_rt.bmp", gfxRender);

    // Game field textures
    // Spielfeld Texturen
    SDL_Texture* f_field = createTexture("gfx/field.bmp", gfxRender);
    SDL_Texture* f_wall = createTexture("gfx/wall.bmp", gfxRender);

    // Collectables
    // Gegenstaende
    SDL_Texture* c_apple = createTexture("gfx/c_apple.bmp", gfxRender);
    SDL_Texture* c_chilli = createTexture("gfx/c_chilli.bmp", gfxRender);
    SDL_Texture* c_strawberry = createTexture("gfx/c_strawberry.bmp", gfxRender);
    SDL_Texture* c_banana = createTexture("gfx/c_banana.bmp", gfxRender);

    // Intital field count on x and y fitting window
    // Setzen der Anzahl der Felder in X und Y welche im Fenster platz haben
    int fieldsX = floor((windowWidth - FIELD_OFFSETX) / FIELD_WIDTH);
    int fieldsY = floor((windowHeight - (FIELD_OFFSETY + UI_OFFSET_Y)) / FIELD_HEIGHT);

    // Initialize random number generator with new seed
    // Initialisierung des Nummern Zufallsgenerators mit neuem Startwert
    srand(rand() % (RAND_MAX - 1));

    // Intitialize game logic
    // Initialisierung der Spiel-Logik
    gameLogic logic;
    logic.lastFood = FOOD_DELAY;
    logic.lastPowerUp = POWERUP_DELAY;
    logic.score = 0;
    logic.itemCount = 0;
    logic.gameSpeed = INITIAL_GAME_SPEED;
    logic.playTime = 0;
    logic.snakes = 0;
    logic.itemAdditions = 0;
    logic.level = 0;
    logic.gameSpeedRamp = 1.0;
    logic.gameMode = SINGLE_PLAYER;

    // Fill the item slots with zeros
    // Die Gegestandesfelder mit Null initialisieren
    for (int i = 0; i < MAX_ITEMS; i++) {
        logic.itemAge[i] = 0;
        logic.itemFields[i] = 0;
        logic.items[i] = 0;
    }

    // Game area definition
    // Spielfeld Definition
    logic.fields = NULL;
    logic.fieldsX = fieldsX;
    logic.fieldsY = fieldsY;
    logic.fieldCount = 0;

    // Game area generation preparation
    // Spielfeld Generierung
    int posX = FIELD_OFFSETX;
    int posY = FIELD_OFFSETY + UI_OFFSET_Y;

    // Reserve memory for game fields
    // Speicher fuer die Spielfelder reservieren
    logic.fields = (field*) realloc(logic.fields, sizeof (field) * (logic.fieldsX * logic.fieldsY));
    if (logic.fields == NULL) {
        printf("ERROR: Could not allocated memory for game field.\n");
        exit(1);
    }

    // Pre-generate the game fields in memory
    // Erstellung der Spielfelder im Speicher
    for (int i = 0; i < fieldsY; i++) {
        for (int j = 0; j < fieldsX; j++) {
            int index = logic.fieldCount;

            logic.fields[index].pX = j;
            logic.fields[index].pY = i;
            logic.fields[index].location = (SDL_Rect){posX, posY, FIELD_WIDTH, FIELD_HEIGHT};
            logic.fields[index].containsItem = false;
            logic.fields[index].itemIndex = 0;

            // Set if the field is a wall
            if (i == 0 || i == logic.fieldsY - 1 || j == 0 || j == logic.fieldsX - 1) {
                logic.fields[index].isWall = true;
            } else {
                logic.fields[index].isWall = false;
            }

            ++logic.fieldCount;
            posX += FIELD_WIDTH;
        }
        posY += FIELD_WIDTH;
        posX = FIELD_OFFSETX;
    }

    // Snake definitions
    // Schlangen Definitionen
    snakeDef* snakes = (snakeDef*) malloc(sizeof (snakeDef) * 2);
    if (snakes == NULL) {
        printf("ERROR: Could not allocated memory for snakes.\n");
        exit(1);
    } else {
        for (int i = 0; i < 2; i++) {
            // Initialize snake
            // Schlange initialisieren
            snakeDef* snake = &snakes[i];
            snake->ateLength = 0;
            snake->hasPowerUp = false;
            snake->powerUp = 0;
            snake->powerUpTime = 0.0;
            snake->pX = 0;
            snake->pY = 0;
            snake->hasNewDirection = false;

            // Set if the snake is in game and snakes initital length
            // Setzen ob die Schlange im Spiel ist und wie lang sie zu beginn ist
            if (i == 0) {
                snake->length = INITIAL_SNAKE_LENGTH;
                snake->isInGame = true;
            } else {
                snake->length = 0;
                snake->isInGame = false;
            }

            // Allocate memory for snake parts
            // Speicher fuer Schlange reservieren
            snake->tail = (snakePart*) malloc(sizeof (snakePart) * INITIAL_SNAKE_LENGTH);

            if (snake->tail == NULL) {
                printf("ERROR: Could not allocated memory for snake parts.\n");
                exit(1);
            }

            // Allocate memory for snakefields
            // Speicher fuer die Schlangenfelder reservieren
            snake->snakeFields = (int*) malloc(sizeof (int) * INITIAL_SNAKE_LENGTH);
            if (snake->snakeFields == NULL) {
                printf("ERROR: Could not allocated memory for snake fields.\n");
                exit(1);
            }

            // Align and create a snake on the game field
            // Schlange auf dem Spielfeld plazieren
            if (snake->isInGame) {
                if (logic.gameMode != SINGLE_PLAYER) {
                    placeSnake(&logic, snakes, i, -1, -1);
                } else {
                    // Load level 1 while SINGLE_PLAYER is set
                    // Lade level 1 weil SINGLE_PLAYER gesetzt ist
                    /*
                    if (!loadLevel(1, &logic, snakes)) {
                        // Fall back if a level could not be loaded
                        // and the snake not beeing placed
                        // Fall back wenn ein Level nicht geladen werden kann
                        // und die Schlange nicht positiert ist
                        placeSnake(&logic, snakes, i, -1, -1);
                    }*/

                    placeSnake(&logic, snakes, i, -1, -1);
                }
            }

            ++logic.snakes;
        }
    }

    // Game loop variables
    // Spielhauptschleifen-Variablen
    SDL_Event userEvent;
    bool runLoop = true;
    bool dead = false;
    bool pauseGame = false;
    bool lvlLoaded = false;

    // Shortcuts
    // Schnellzugriffe
    snakePart* head = NULL;
    int headPrevPX = 0, headPrevPY = 0;
    int randomIndex = 0, itemIndex = 0;
    char deathReason[128];
    int deathReasonMsg = 0;

    SDL_Texture* currentTexture = NULL;
    SDL_Texture* itemTexture = NULL;

    SDL_Texture* scoreTexture = NULL;
    SDL_Texture* timeTexture = NULL;
    SDL_Texture* lengthTexture = NULL;
    SDL_Texture* deathTexture = NULL;
    SDL_Texture* deathReasonTexture = NULL;

    // Set logic timestamp when the game has been started for playtime display
    // Zeit erfassen wann das Spiel gestartet wurde fuer Spielzeit Anzeige
    logic.startTime = time(NULL);

    // Game loop
    // Spielschleife
    while (runLoop) {

        // Process window events and game controls
        // Abarbeitung der Ereignisse und Spiel- und Fensterkontrollen
        while (SDL_PollEvent(&userEvent)) {
            if (userEvent.type == SDL_WINDOWEVENT && userEvent.window.event == SDL_WINDOWEVENT_CLOSE) {
                runLoop = false;
            } else if (userEvent.type == SDL_KEYDOWN) {
                snakeDef* snake = &snakes[0];

                // General controls and control check which snake to use
                // Generelle Spielkontrollen und Pruefung welche Schlange
                // die Bewegung erhalten soll
                switch (userEvent.key.keysym.scancode) {
                    case SDL_SCANCODE_P:
                        pauseGame = !pauseGame;
                        break;
                    case SDL_SCANCODE_R:
                        dead = false;
                        resetGame(&logic, snakes);
                        break;
                    case SDL_SCANCODE_W:
                    case SDL_SCANCODE_A:
                    case SDL_SCANCODE_S:
                    case SDL_SCANCODE_D:
                        // If there is a 2nd snake in game, set controls to this snake
                        // otherwise use the snake at null
                        // Test ob Schlange 1 im Spiel ist, falls ja soll Schlage auf eins
                        // bewegt werden, sonst bleibt die Kontrolle bei Schlange Null
                        if (snakes[1].isInGame) {
                            snake = &snakes[1];
                        }
                        break;
                    default:
                        break;
                }

                // Move snake head in key direction
                // Bewegeung des Schlangenkopfes aufgrund der Richtungstaste
                switch (userEvent.key.keysym.scancode) {
                    case SDL_SCANCODE_W:
                    case SDL_SCANCODE_UP:
                        if (snake->tail[0].orientation == TOP || snake->hasNewDirection || dead)
                            continue;

                        if (snake->tail[0].orientation != BOTTOM) {
                            snake->tail[0].orientation = TOP;
                            snake->hasNewDirection = true;
                        }

                        break;
                    case SDL_SCANCODE_A:
                    case SDL_SCANCODE_LEFT:
                        if (snake->tail[0].orientation == LEFT || snake->hasNewDirection || dead)
                            continue;

                        if (snake->tail[0].orientation != RIGHT) {
                            snake->tail[0].orientation = LEFT;
                            snake->hasNewDirection = true;
                        }

                        break;
                    case SDL_SCANCODE_D:
                    case SDL_SCANCODE_RIGHT:
                        if (snake->tail[0].orientation == RIGHT || snake->hasNewDirection || dead)
                            continue;

                        if (snake->tail[0].orientation != LEFT) {
                            snake->tail[0].orientation = RIGHT;
                            snake->hasNewDirection = true;
                        }

                        break;
                    case SDL_SCANCODE_S:
                    case SDL_SCANCODE_DOWN:
                        if (snake->tail[0].orientation == BOTTOM || snake->hasNewDirection || dead)
                            continue;

                        if (snake->tail[0].orientation != TOP) {
                            snake->tail[0].orientation = BOTTOM;
                            snake->hasNewDirection = true;
                        }

                        break;
                    case SDL_SCANCODE_KP_PLUS:
                        //++snake->ateLength;
                        break;
                    default:
                        continue;
                        break;
                }
            }
        }

        // If the the game is paused, jump back to loop head
        // Wenn das Spiel pausiert ist, gehe zum Schleifen Kopf
        if (pauseGame) {
            continue;
        }

        if (runLoop) {
            SDL_RenderClear(gfxRender);
            clock_t clockBefore = clock();

            if (!dead) {

                // Spawing of collectables/items if below MAX_ITEMS
                // Spawn food and items
                // Platzierung von Gegenstaenden und Nahrung wenn unter MAX_ITEMS
                if (logic.lastFood == 0 && logic.itemCount < MAX_ITEMS) {

                    // Seed random number generator
                    // Zufallsgenerator neu initialisieren
                    srand(rand() % (RAND_MAX - 1));

                    logic.lastFood = FOOD_DELAY;
                    int item = APPLE;

                    // Change the item every 15th, 12th, 8th time to an chilli,
                    // banane, strawberry spawning
                    // Aendere alle 15, 12 ider 8 mal den Gegenstand zu einer
                    // Chilli, Banane oder Erdbeere
                    if (logic.itemAdditions != 0) {
                        if (logic.itemAdditions % 14 == 0) {
                            item = CHILLI;
                        } else if (logic.itemAdditions % 11 == 0) {
                            item = BANANA;
                        } else if (logic.itemAdditions % 7 == 0) {
                            item = STRAWBERRY;
                        }
                    }

                    // Set limit for logic itemAdditions
                    // Setze ein Limit fuer den insgesamt platzierten
                    // Gegestaendszaehler
                    if (logic.itemAdditions >= 101) {
                        logic.itemAdditions = 0;
                    }

                    // Get free random field and set field to contain the item
                    // Eine freies Zufallsfeld ermittels und den Gegenstand dort
                    // platzieren
                    randomIndex = getAvailableRandomFieldIndex(&logic, snakes);
                    if (randomIndex != -1) {
                        logic.fields[randomIndex].containsItem = true;
                        logic.fields[randomIndex].itemIndex = logic.itemCount;

                        itemIndex = logic.itemCount;
                        logic.itemFields[itemIndex] = randomIndex;
                        logic.items[itemIndex] = item;
                        logic.itemAge[itemIndex] = FOOD_DECAY;

                        ++logic.itemCount;
                        ++logic.itemAdditions;
                    }
                } else if (logic.itemCount == MAX_ITEMS) {
                    logic.lastFood = FOOD_DELAY;
                    logic.lastPowerUp = POWERUP_DELAY;

                }


                // Process snake heads movement, item pickup, tail movement,
                // and dead checks for snakes in logic
                // Ausfuehrung der Bewegung der Schlange, Aufsammeln eines
                // Gegenstandes oder ob die Schlange sich beisst/in eine Wand rennt
                for (int i = 0; i < logic.snakes; i++) {
                    if (!snakes[i].isInGame) {
                        continue;
                    }

                    // Set the snake we are working on
                    // Setze die Schlange mit der wir arbeiten
                    snakeDef* snake = &snakes[i];

                    // Head movement and updates
                    // Kopf-Bewegung und Aktualisierung
                    head = &snake->tail[0];
                    // Move head
                    headPrevPX = head->pX;
                    headPrevPY = head->pY;

                    switch (head->orientation) {
                        case LEFT:
                            --head->pX;
                            break;
                        case RIGHT:
                            ++head->pX;
                            break;
                        case TOP:
                            --head->pY;
                            break;
                        case BOTTOM:
                            ++head->pY;
                            break;
                        default:
                            break;
                    }

                    // Update head location
                    // Aktualisierung der Kopf Position
                    head->location.x = calcPointToPixel(head->pX, false);
                    head->location.y = calcPointToPixel(head->pY, true);
                    snake->snakeFields[0] = getFieldIndex(head->pX, head->pY, &logic);

                    snakes->hasNewDirection = false;

                    // Check if field with head location contains an item
                    // Enthaelt die neue Kopfposition einen Gegenstand?
                    int gameField = snake->snakeFields[0];
                    if (logic.fields[gameField].containsItem) {
                        int index = logic.fields[gameField].itemIndex;

                        logic.fields[gameField].containsItem = false;
                        logic.fields[gameField].itemIndex = 0;

                        switch (logic.items[index]) {
                            case APPLE:
                                ++snake->ateLength;

                                // Variable score addition based on snake length
                                // Variable Punkteaddition basierend auf der Laenge
                                // der Schlange
                                logic.score += floor(1.5 * snake->length);

                                // Increase game speed, reducing SDL_Delay time
                                // Spielgeschwindigkeit erhoehen durch herabsetzen
                                // der Wartezeit in SDL_Delay
                                if (logic.gameSpeed > MINIMUM_GAME_SPEED) {
                                    logic.gameSpeed -= GAME_SPEED_DECREASE;
                                }

                                //printf("Updated score: %d\n", logic.score);
                                break;
                            case STRAWBERRY:
                                ++snake->ateLength;
                                logic.score += floor(2.5 * snake->length);

                                if (logic.gameSpeed > MINIMUM_GAME_SPEED) {
                                    logic.gameSpeed -= GAME_SPEED_DECREASE;
                                }
                                break;
                            case BANANA:
                                ++snake->ateLength;
                                logic.score += floor(3.5 * snake->length);

                                if (logic.gameSpeed > MINIMUM_GAME_SPEED) {
                                    logic.gameSpeed -= GAME_SPEED_DECREASE;
                                }
                                break;
                            case CHILLI:
                                ++snake->ateLength;
                                logic.score += floor(5 * snake->length);

                                if (logic.gameSpeed > MINIMUM_GAME_SPEED) {
                                    logic.gameSpeed -= GAME_SPEED_DECREASE;
                                }
                                break;
                            default:
                                break;
                        }

                        // Shift uncollected items one ahead in the item list
                        // Gegenstandsspeicher um eines nach vorner verschieben
                        if (index < (MAX_ITEMS - 1) && logic.itemCount > 1) {
                            memmove(&logic.itemFields[index], &logic.itemFields[index + 1], sizeof (int) * (MAX_ITEMS - (index + 1)));
                            memmove(&logic.itemAge[index], &logic.itemAge[index + 1], sizeof (int) * (MAX_ITEMS - (index + 1)));
                            memmove(&logic.items[index], &logic.items[index + 1], sizeof (int) * (MAX_ITEMS - (index + 1)));
                        }

                        logic.itemCount--;

                        // Update item slots for item fields due to previous shift
                        // Aktualisierung der Gegenstands-Felder aufgrund des
                        // vorhergehenden Speicher-Shifts
                        for (int i = 0; i < logic.itemCount; i++) {
                            logic.fields[logic.itemFields[i]].itemIndex = i;
                        }
                    }

                    // Movement of snake tail or snake extension
                    // Bewegung des Schlangenkoerpers oder Schlangen Erweiterung
                    if (snake->ateLength == 0) {
                        // Snake didnt eat, move parts ahead
                        // Schlage hat nicht gegessen, ziehe alle Teile eins vor
                        snakePart* currentPart = NULL;
                        for (int i = snake->length - 1; i > 1; i--) {
                            currentPart = &snake->tail[i];
                            currentPart->pX = currentPart->previous->pX;
                            currentPart->pY = currentPart->previous->pY;
                            currentPart->orientation = currentPart->previous->orientation;
                            currentPart->corner = currentPart->previous->corner;
                            currentPart->location.x = calcPointToPixel(currentPart->pX, false);
                            currentPart->location.y = calcPointToPixel(currentPart->pY, true);
                            snake->snakeFields[i] = getFieldIndex(currentPart->pX, currentPart->pY, &logic);
                        }

                        // Second part special handling since we need the old head
                        // coordinates for movement instead of new ones
                        // Der erste Schwanz Teil wird gesondert auf die alte
                        // Kopfposition gelegt
                        currentPart = &snake->tail[1];
                        currentPart->pX = headPrevPX;
                        currentPart->pY = headPrevPY;
                        currentPart->orientation = currentPart->previous->orientation;
                        currentPart->corner = currentPart->previous->corner;
                        currentPart->location.x = calcPointToPixel(currentPart->pX, false);
                        currentPart->location.y = calcPointToPixel(currentPart->pY, true);
                        snake->snakeFields[1] = getFieldIndex(currentPart->pX, currentPart->pY, &logic);
                    }

                    // Check for death reasons
                    // Ueberpruefung auf Todesgruende
                    int headFieldIndex = getFieldIndex(head->pX, head->pY, &logic);

                    // Is the current head location a wall part?
                    // Ist die aktuelle Kopfposition eine Wand?
                    if (logic.fields[headFieldIndex].isWall) {
                        deathReasonMsg = MSG_WALL;
                        dead = true;
                    } else {
                        // Is the head field a snake tail field (bite)?
                        // Ist die Kopfposition ein Schlagenabschnitt (Biss)?
                        for (int i = 1; i < snake->length; i++) {
                            if (headFieldIndex == snake->snakeFields[i]) {
                                deathReasonMsg = MSG_BITE;
                                dead = true;
                                break;
                            }
                        }
                    }

                    if (snake->ateLength != 0) {
                        // Snake ate, create new snake part
                        // Die Schlange hat gefressen, Erweiterung der Schlange
                        --snake->ateLength;
                        extendSnake(&logic, snake, headPrevPX, headPrevPY);

                        // Reset snakes
                        if (logic.gameMode == SINGLE_PLAYER) {
                            if (snake->length >= (logic.level * 2) + 33) {
                                snakeDef* snake = &snakes[0];
                                snake->ateLength = 0;
                                snake->hasNewDirection = false;
                                snake->hasPowerUp = false;
                                snake->length = 3;
                                snake->powerUp = 0;
                                snake->powerUpTime = 0;

                                // Reset game fields
                                for (i = 0; i < logic.fieldCount; i++) {
                                    logic.fields[i].containsItem = false;
                                    logic.fields[i].isWall = false;
                                    logic.fields[i].itemIndex = 0;

                                    if (logic.fields[i].pY == 0 || logic.fields[i].pY == logic.fieldsY - 1 || logic.fields[i].pX == 0 || logic.fields[i].pX == logic.fieldsX - 1) {
                                        logic.fields[i].isWall = true;
                                    } else {
                                        logic.fields[i].isWall = false;
                                    }
                                }

                                // Reset item holders
                                for (i = 0; i < (MAX_ITEMS + 1); i++) {
                                    logic.itemFields[i] = 0;
                                    logic.itemAge[i] = 0;
                                    logic.items[i] = 0;
                                }

                                if (!loadLevel(logic.level + 1, &logic, snakes)) {
                                    logic.level = 0;
                                    placeSnake(&logic, snakes, 0, -1, -1);
                                } else {
                                    ++logic.level;
                                }

                                logic.gameSpeedRamp = 0.15;
                                lvlLoaded = true;

                                // Reset parts of the logic
                                // Teile der Logik resetten fuer Levelwechsel
                                logic.itemCount = 0;
                                logic.lastFood = FOOD_DELAY;
                                logic.lastPowerUp = POWERUP_DELAY;
                                logic.gameSpeed = INITIAL_GAME_SPEED;
                                logic.itemAdditions = 0;


                                break;
                            }

                        }

                    }
                }
            }

            if (lvlLoaded) {
                if (logic.gameSpeedRamp < 1) {
                    logic.gameSpeedRamp += 0.05;
                } else {
                    logic.gameSpeedRamp = 1.0;
                    lvlLoaded = false;
                }
            }

            // Rendering
            // Queue rendering for all game fields
            // Puffern der Spielfelder fuer die Renderausgabe
            for (int i = 0; i < logic.fieldCount; i++) {
                if (logic.fields[i].isWall) {
                    SDL_RenderCopy(gfxRender, f_wall, NULL, &logic.fields[i].location);
                } else {
                    SDL_RenderCopy(gfxRender, f_field, NULL, &logic.fields[i].location);
                }
            }

            // Queueing of items for rendering
            // Puffern der Gegenstaende fuer die Ausgabe
            for (int i = 0; i < logic.itemCount; i++) {
                switch (logic.items[i]) {
                    case APPLE:
                    default:
                        itemTexture = c_apple;
                        break;
                    case CHILLI:
                        itemTexture = c_chilli;
                        break;
                    case STRAWBERRY:
                        itemTexture = c_strawberry;
                        break;
                    case BANANA:
                        itemTexture = c_banana;
                        break;
                }

                SDL_RenderCopy(gfxRender, itemTexture, NULL, &logic.fields[logic.itemFields[i]].location);
            }

            // Queue render snake for display
            // Puffern der Schlagen fuer Ausgabe
            for (int j = 0; j < logic.snakes; j++) {

                if (!snakes[j].isInGame) {
                    continue;
                }

                snakeDef* snake = &snakes[j];
                for (int i = snake->length - 1; i != -1; i--) {
                    // Set the right textures/images for the current snake part
                    // Setzen der richtigen Texture fuer den aktuell Abschnitt
                    if (snake->tail[i].previous == NULL) {
                        // Its the head
                        // Fuer den Kopf
                        switch (snake->tail[i].orientation) {
                            case LEFT:
                            default:
                                currentTexture = head_l;
                                break;
                            case RIGHT:
                                currentTexture = head_r;
                                break;
                            case TOP:
                                currentTexture = head_t;
                                break;
                            case BOTTOM:
                                currentTexture = head_b;
                                break;
                        }
                    } else if (snake->tail[i].next == NULL) {
                        // Its the tail end
                        // Fuer das Schwanzende
                        switch (snake->tail[i].orientation) {
                            case LEFT:
                            default:
                                currentTexture = tail_r;
                                break;
                            case RIGHT:
                                currentTexture = tail_l;
                                break;
                            case TOP:
                                currentTexture = tail_b;
                                break;
                            case BOTTOM:
                                currentTexture = tail_t;
                                break;
                        }
                    } else {
                        // Its a in between part
                        // Fuer Mittelteile
                        snakePart* prev = snake->tail[i].previous;
                        snakePart* next = snake->tail[i].next;

                        if (abs(prev->pX - next->pX) == 2) {
                            // Horizontal
                            currentTexture = sp_hor;
                        } else if (abs(prev->pY - next->pY) == 2) {
                            // Veritcal
                            // Vertikal
                            currentTexture = sp_ver;
                        } else {
                            snakePart* cur = &snake->tail[i];

                            // Existing corner processing
                            // Ecken
                            if (cur->corner != 0) {
                                switch (cur->corner) {
                                    case CLB:
                                        currentTexture = sp_clb;
                                        break;
                                    case CLT:
                                        currentTexture = sp_clt;
                                        break;
                                    case CRB:
                                        currentTexture = sp_crb;
                                        break;
                                    case CRT:
                                        currentTexture = sp_crt;
                                        break;
                                    default:
                                        break;
                                }
                            } else {
                                // New corner creation
                                // Erstellung einer neuen Ecke
                                if ((prev->orientation == LEFT && next->orientation == TOP) || (prev->orientation == BOTTOM && next->orientation == RIGHT)) {
                                    currentTexture = sp_clb;
                                    cur->corner = CLB;
                                } else if ((prev->orientation == LEFT && next->orientation == BOTTOM) || (prev->orientation == TOP && next->orientation == RIGHT)) {
                                    currentTexture = sp_clt;
                                    cur->corner = CLT;
                                } else if ((prev->orientation == RIGHT && next->orientation == TOP) || (prev->orientation == BOTTOM && next->orientation == LEFT)) {
                                    currentTexture = sp_crb;
                                    cur->corner = CRB;
                                } else if ((prev->orientation == RIGHT && next->orientation == BOTTOM) || (prev->orientation == TOP && next->orientation == LEFT)) {
                                    currentTexture = sp_crt;
                                    cur->corner = CRT;
                                }
                            }
                        }
                    }

                    // Set texture for rendering at location
                    // Texture zum rendern and Stelle platzieren
                    SDL_RenderCopy(gfxRender, currentTexture, NULL, &snake->tail[i].location);
                }
            }

            // Render UI components on top
            // Ausgabe der UI Komponenten/Anzeigen

            // Score
            // Punktestand
            sprintf(displayText, "SCORE %d PTS", logic.score);
            scoreboard = TTF_RenderText_Solid(rw14, displayText, uiFontColor);
            scoreRect = (SDL_Rect){((windowWidth - scoreboard->w) / 2), 11, scoreboard->w, scoreboard->h};
            scoreTexture = SDL_CreateTextureFromSurface(gfxRender, scoreboard);
            SDL_RenderCopy(gfxRender, scoreTexture, NULL, &scoreRect);

            SDL_DestroyTexture(scoreTexture);
            SDL_FreeSurface(scoreboard);
            scoreboard = NULL;

            // Playtime
            // Spielzeit
            sprintf(displayText, "TIME %u sec", logic.playTime);
            timeboard = TTF_RenderText_Solid(rw14, displayText, uiFontColor);
            timeRect = (SDL_Rect){20, 11, timeboard->w, timeboard->h};
            timeTexture = SDL_CreateTextureFromSurface(gfxRender, timeboard);
            SDL_RenderCopy(gfxRender, timeTexture, NULL, &timeRect);

            SDL_DestroyTexture(timeTexture);
            SDL_FreeSurface(timeboard);
            timeboard = NULL;

            // Length
            // Laenge
            sprintf(displayText, "LENGTH %d feet", snakes[0].length - INITIAL_SNAKE_LENGTH);
            lengthboard = TTF_RenderText_Solid(rw14, displayText, uiFontColor);
            lengthRect = (SDL_Rect){windowWidth - 140, 11, lengthboard->w, lengthboard->h};
            lengthTexture = SDL_CreateTextureFromSurface(gfxRender, lengthboard);
            SDL_RenderCopy(gfxRender, lengthTexture, NULL, &lengthRect);

            SDL_DestroyTexture(lengthTexture);
            SDL_FreeSurface(lengthboard);
            lengthboard = NULL;

            if (dead) {
                // Overlay background for death display
                // Hintergrund der Todesanzeige
                SDL_SetRenderDrawColor(gfxRender, 215, 0, 0, 200);
                SDL_SetRenderDrawBlendMode(gfxRender, SDL_BLENDMODE_BLEND);
                SDL_Rect deathBackground = {80, 80, windowWidth - 160, windowHeight - 160};
                SDL_RenderFillRect(gfxRender, &deathBackground);

                // Reset renderer mode
                // Ausgabe Modos zuruecksetzen
                SDL_SetRenderDrawBlendMode(gfxRender, SDL_BLENDMODE_NONE);
                SDL_SetRenderDrawColor(gfxRender, 0, 0, 0, 1);

                // Death text display
                // Todestext Anzeige
                deathboard = TTF_RenderText_Solid(rw58, "ALL SNAPPED!", uiFontColor);
                deathRect = (SDL_Rect){((windowWidth - deathboard->w) / 2), 155, deathboard->w, deathboard->h};
                deathTexture = SDL_CreateTextureFromSurface(gfxRender, deathboard);
                SDL_RenderCopy(gfxRender, deathTexture, NULL, &deathRect);

                SDL_DestroyTexture(deathTexture);
                SDL_FreeSurface(deathboard);
                deathboard = NULL;

                // Death reason message
                // Todesgrund Auswahl
                switch (deathReasonMsg) {
                    case MSG_WALL:
                        deathReason[0] = '\0';
                        strcat(deathReason, "You crashed into a wall!");
                        break;
                    case MSG_BITE:
                        deathReason[0] = '\0';
                        strcat(deathReason, "Don't eat yourself");
                        break;
                    default:
                        deathReason[0] = '\0';
                        strcat(deathReason, "You died for no obvious reasons.");
                        break;
                }

                // Death reason text display
                // Todesgrund Textanzeige
                deathReasonboard = TTF_RenderText_Solid(rw14, deathReason, uiFontColor);
                reasonRect = (SDL_Rect){((windowWidth - deathReasonboard->w) / 2), 240, deathReasonboard->w, deathReasonboard->h};
                deathReasonTexture = SDL_CreateTextureFromSurface(gfxRender, deathReasonboard);
                SDL_RenderCopy(gfxRender, deathReasonTexture, NULL, &reasonRect);

                SDL_DestroyTexture(deathReasonTexture);
                SDL_FreeSurface(deathReasonboard);
                deathReasonboard = NULL;

            } else {
                // Update game logic for items after calculation
                // Aktualisierung der Spiel-Logik fuer Gegenstaende nach Berechnung

                // Update item counters
                // Aktualisierung der Gegenstands-Zaehler
                if (logic.itemCount < MAX_ITEMS) {
                    --logic.lastFood;
                    --logic.lastPowerUp;
                }

                // Update game play time
                // Aktualisuerng der Zeit
                logic.playTime = time(NULL) - logic.startTime;
            }

            // Render everything staged by RenderCopy
            // Alles ausgeben was gepuffert wurde durch RenderCopy
            SDL_RenderPresent(gfxRender);

            clock_t clockAfter = clock();

            // Delay further rendering/execution of game loop
            // Ausfuehrung der Spielschleife verzoegern
            SDL_Delay(ceil((logic.gameSpeed - (abs(clockAfter - clockBefore) * 0.001)) / logic.gameSpeedRamp));
        }
    }

    // Destroying font
    // Schriften aus dem Speicher entfernen
    TTF_CloseFont(rw14);
    TTF_CloseFont(rw58);

    // Destroying textures
    // Texturen aus dem Speicher entfernen

    // Snake gfx
    // Schlangen Grafiken
    SDL_DestroyTexture(head_l);
    SDL_DestroyTexture(head_t);
    SDL_DestroyTexture(head_r);
    SDL_DestroyTexture(head_b);
    SDL_DestroyTexture(tail_l);
    SDL_DestroyTexture(tail_t);
    SDL_DestroyTexture(tail_r);
    SDL_DestroyTexture(tail_b);
    SDL_DestroyTexture(sp_hor);
    SDL_DestroyTexture(sp_ver);
    SDL_DestroyTexture(sp_clb);
    SDL_DestroyTexture(sp_clt);
    SDL_DestroyTexture(sp_crb);
    SDL_DestroyTexture(sp_crt);

    // Field textures
    // Feld Texturen
    SDL_DestroyTexture(f_field);
    SDL_DestroyTexture(f_wall);

    // Collectables
    // Gegenstaende
    SDL_DestroyTexture(c_apple);
    SDL_DestroyTexture(c_chilli);
    SDL_DestroyTexture(c_strawberry);
    SDL_DestroyTexture(c_banana);

    // Destroy ui interface elements
    // UI Elemente
    if (scoreboard != NULL) {
        SDL_FreeSurface(scoreboard);
    }

    if (timeboard != NULL) {
        SDL_FreeSurface(timeboard);
    }

    if (lengthboard != NULL) {
        SDL_FreeSurface(lengthboard);
    }

    if (deathboard != NULL) {
        SDL_FreeSurface(deathboard);
    }

    if (deathReasonboard != NULL) {
        SDL_FreeSurface(deathReasonboard);
    }

    // Destroying renderer and window
    // Den Renderer und das Fenster zerstoeren
    SDL_DestroyRenderer(gfxRender);
    SDL_DestroyWindow(snakeWindow);

    // Ressource freeing
    // Ressourcen freigeben

    // Logic
    // Logik
    free(logic.fields);

    // Snake
    // Schlange
    for (int i = 0; i < logic.snakes; i++) {
        free(snakes[i].snakeFields);
        free(snakes[i].tail);

    }

    // Snakes
    // Schlangen
    free(snakes);

    return 0;
}
