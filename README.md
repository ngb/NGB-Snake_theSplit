INTRODUCTION

My first project on Gitlab, I am excited... Snake, yes.
Some would call it Nibbles too. ;)

Its written in C using SDL2 and SDL2 TTF font library.
It uses Raleway as font to display UI items.

HOW TO PLAY

For Windows 32 and 64 bit there are two zip archives in the folder "builds" containing all data required to play.
Those versions contain SDL2 as well as SDL2 TTF library dlls and there requirements, so no additional download is required.

To start the game simply download, extract everything and launch "snakebite.exe" and you should be good to go.

Please note that for other builds I would only provide the new executables as the repository otherwise grows very large quickly.
So if you havent started yet, download the zip file version of v0.91 - and replace/overwrite the executable game file with any newer version you like.

If anything else changes, I will try to keep information updated in here or in the changelog.


REQUIREMENTS

Snakebite alias snake is linked to SDL2 and SDL2 TTF - which is required for compiling the c source.
I will try to get binaries for Linux and Windows ready of the app, so one only needs to download the DLL files/binaries.

SDL2 can be obtained at http://www.libsdl.org for Windows, Mac OSX and Linux.
For Windows and Mac OS, binaries for 32bit and 64bit can be downloaded at: https://www.libsdl.org/download-2.0.php
Installation instructions for Linux and Mac OS can be found at: https://wiki.libsdl.org/Installation

SDl2 TTF font library can also be downloaded for Windows (32bit/64bit) and Mac OS at:
https://www.libsdl.org/projects/SDL_ttf/

SDL2 TTF font library installation or compiling instructions can be found at:
https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html

One ressource to find Raleway is (but its packaged with the app already):
http://www.fontsquirrel.com/fonts/raleway


If you have questions or feature suggestions, please let me know :)